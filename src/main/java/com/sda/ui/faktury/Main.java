package com.sda.ui.faktury;


import com.sda.ui.faktury.models.*;
import com.sda.ui.faktury.views.MainPanel;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {

//        JFrame okno = new JFrame("Manager faktur");
//        okno.setContentPane(new MainPanel().getMain());
//        okno.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
//        okno.setPreferredSize(new Dimension(800, 640));
//        okno.setSize(new Dimension(800, 640));
//        okno.setVisible(true);

        ModelDao modelDao = new ModelDao();
        Invoice invoice = new Invoice();

        Company company = new Company();
        modelDao.add(company);

        Client client = new Client();
        modelDao.add(client);


        invoice.setClient(client);
        invoice.setCompany(company);
        modelDao.add(invoice);


        InvoiceEntry invoiceEntry = new InvoiceEntry();
        invoiceEntry.setName("Bułki");
        invoiceEntry.setPrice(5.0);
        invoiceEntry.setInvoice(invoice);
//        invoice.getInvoiceEntryList().add(invoiceEntry);

        modelDao.add(invoiceEntry);
//        modelDao.add(invoice);

    }
}
