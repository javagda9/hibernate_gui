package com.sda.ui.faktury.views;

import com.sda.ui.faktury.models.Company;

import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;

public class CompanyTableModel extends DefaultTableModel {
    private final static String[] columnNames = new String[]{"Id", "Name"};
    private List<Company> companyList;

    public CompanyTableModel() {
        super();
        companyList = new ArrayList<>();
    }
    public void addCompany(Company c){
        companyList.add(c);

        fireTableDataChanged();
    }

    @Override
    public Object getValueAt(int row, int column) {
        if (column == 0) {
            return companyList.get(row).getId();
        } else if (column == 1) {
            return companyList.get(row).getName();
        }

        return "Unknown";
    }

    @Override
    public int getRowCount() {
        if(companyList == null){
            return 0;
        }
        return companyList.size();
    }


    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
}
