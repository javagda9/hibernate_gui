package com.sda.ui.faktury.views;

import com.sda.ui.faktury.models.Company;

import javax.swing.*;

public class MainPanel {
    private JPanel main;
    private JList list1;
    private JTable table1;

    public MainPanel() {
        //##############
        DefaultListModel<Company> model = new DefaultListModel<>();
//        model.addElement(new Company(1, "a"));
//        model.addElement(new Company(2, "b"));
//        model.addElement(new Company(3, "c"));
//        model.addElement(new Company(4, "d"));


        list1.setModel(model);
        //################
        CompanyTableModel modelTable = new CompanyTableModel();
        table1.setModel(modelTable);

//        modelTable.addCompany(new Company(7, "7awfgawfawfawf"));

    }

    public JPanel getMain() {
        return main;
    }
}
