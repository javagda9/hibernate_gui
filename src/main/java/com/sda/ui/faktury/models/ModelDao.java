package com.sda.ui.faktury.models;

import org.hibernate.Session;
import org.hibernate.SessionException;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

public class ModelDao {
    public void add(Invoice i) {
        SessionFactory factory = HibernateUtility.getSessionFactory();
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.persist(i);
            transaction.commit();
        } catch (SessionException se) {
            transaction.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void add(Company c) {
        SessionFactory factory = HibernateUtility.getSessionFactory();
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.persist(c);
            transaction.commit();
        } catch (SessionException se) {
            transaction.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void add(Client c) {
        SessionFactory factory = HibernateUtility.getSessionFactory();
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.persist(c);
            transaction.commit();
        } catch (SessionException se) {
            transaction.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }

    public void add(InvoiceEntry e) {
        SessionFactory factory = HibernateUtility.getSessionFactory();
        Session session = factory.openSession();
        Transaction transaction = session.beginTransaction();

        try {
            session.persist(e);
            transaction.commit();
        } catch (SessionException se) {
            transaction.rollback();
        } finally {
            if (session != null) {
                session.close();
            }
        }
    }
}
