package com.sda.ui.faktury.models;

import javax.persistence.*;

@Entity
public class InvoiceEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private double price;

    @ManyToOne
    private Invoice invoice;

    public InvoiceEntry() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public void setInvoice(Invoice invoice) {
        this.invoice = invoice;
    }

    @Override
    public String toString() {
        return "InvoiceEntry{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", invoice=" + invoice +
                '}';
    }
}

