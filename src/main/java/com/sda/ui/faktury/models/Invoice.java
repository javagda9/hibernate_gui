package com.sda.ui.faktury.models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Invoice {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Client client;

    @ManyToOne
    private Company company;

    @OneToMany(mappedBy = "invoice")
    private List<InvoiceEntry> invoiceEntryList;

    public Invoice() {
        invoiceEntryList = new ArrayList<>();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public List<InvoiceEntry> getInvoiceEntryList() {
        return invoiceEntryList;
    }

    public void setInvoiceEntryList(List<InvoiceEntry> invoiceEntryList) {
        this.invoiceEntryList = invoiceEntryList;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", client=" + client +
                ", company=" + company +
                '}';
    }
}
