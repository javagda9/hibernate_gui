package com.sda.ui.game;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

public class MainPanel {
    private JPanel main;
    private JPanel panelNorth;
    private JPanel panelSouth;
    private JPanel panelCenter;
    private JButton buttonGenruj;
    private JTextField inputLiczba;
    private JButton buttonZgadnij;
    private JLabel etykietaLicznik;
    private JLabel etykietaStatus;
    private JLabel etykieta;

    //#################
    private Integer wylosowanaLiczba = null;
    private int licznikPrób = 0;

    public MainPanel() {
        buttonGenruj.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                int wylosowana = new Random().nextInt(100);

                wylosowanaLiczba = wylosowana;
                inputLiczba.setEnabled(true);
                buttonZgadnij.setEnabled(true);
                licznikPrób = 0;
                etykietaLicznik.setText("" + licznikPrób);
                inputLiczba.setText("");
                super.mousePressed(e);
            }
        });

        buttonZgadnij.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String wejscieUzytkownika = inputLiczba.getText();
                licznikPrób++;

                try {
                    etykietaLicznik.setText("" + licznikPrób);
                    int wejscie = Integer.parseInt(wejscieUzytkownika);

                    if (wejscie > wylosowanaLiczba) {
                        etykietaStatus.setText("Liczba jest mniejsza!");
                    } else if (wejscie < wylosowanaLiczba) {
                        etykietaStatus.setText("Liczba jest większa!");
                    } else if (wejscie == wylosowanaLiczba) {
                        etykietaStatus.setText("Odgadłeś!");
                        inputLiczba.setEnabled(false);
                        buttonZgadnij.setEnabled(false);
                    }
                } catch (NumberFormatException nfe) {
                    System.err.println("Error parsing");
                    etykietaStatus.setText("Niepoprawne wejście!");
                    return;
                }
                super.mouseClicked(e);
            }
        });
    }

    public JPanel getMain() {
        return main;
    }
}
