package com.sda.ui.game;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {

        JFrame okno = new JFrame("Manager faktur");
        okno.setContentPane(new MainPanel().getMain());
        okno.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        okno.setPreferredSize(new Dimension(800, 640));
        okno.setSize(new Dimension(800, 640));
        okno.setVisible(true);

    }
}

